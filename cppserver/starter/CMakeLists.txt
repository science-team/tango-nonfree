cmake_minimum_required(VERSION 3.7 FATAL_ERROR)
project(Starter)

if(NOT CMAKE_BUILD_TYPE)
    message("No build type specified - default is DEBUG")
    set(CMAKE_BUILD_TYPE DEBUG)
endif()

include(FindPkgConfig)

pkg_search_module(TANGO_PKG REQUIRED tango)

set(SOURCES CheckProcessUtil.cpp
            ClassFactory.cpp
            main.cpp
            PingThread.cpp
            Starter.cpp
            StarterClass.cpp
            StarterService.cpp
            StarterStateMachine.cpp
            StarterUtil.cpp
            StartProcessThread.cpp)

include_directories(${CMAKE_SOURCE_DIR} ${TANGO_PKG_INCLUDE_DIRS})
link_directories(${TANGO_PKG_LIBRARY_DIRS})

add_executable(Starter ${SOURCES} ${ADDITIONAL_SOURCES})
target_link_libraries(Starter ${TANGO_PKG_LIBRARIES} -Wl,-z,now -pie)
target_compile_options(Starter PUBLIC ${TANGO_PKG_CFLAGS_OTHER} -Wall -Wextra -D_FORTIFY_SOURCE=2 -O1 -fpie)

install(TARGETS Starter
        RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
        CONFIGURATIONS ${CMAKE_BUILD_TYPE})
