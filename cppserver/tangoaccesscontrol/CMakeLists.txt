cmake_minimum_required(VERSION 3.7 FATAL_ERROR)
project(TangoAccessControl)

if(NOT CMAKE_BUILD_TYPE)
    message("No build type specified - default is DEBUG")
    set(CMAKE_BUILD_TYPE DEBUG)
endif()

include(FindPkgConfig)
include(configure/FindMySQL.cmake)

if(NOT MYSQL_FOUND)
    message(SEND_ERROR " Can not find MySQL client library.")
endif()

pkg_search_module(TANGO_PKG REQUIRED tango)

set(SOURCES ClassFactory.cpp
            DbUtils.cpp
            TangoAccessControlStateMachine.cpp
            TangoAccessControl.cpp
            TangoAccessControlClass.cpp
            main.cpp)

set(ADDITIONAL_SOURCES ../AbstractClasses/AccessControl.cpp
                       ../AbstractClasses/AccessControlClass.cpp
                       ../AbstractClasses/AccessControlStateMachine.cpp)

include_directories(${CMAKE_SOURCE_DIR} ../AbstractClasses ${TANGO_PKG_INCLUDE_DIRS} ${MYSQL_INCLUDE_DIRS})
link_directories(${TANGO_PKG_LIBRARY_DIRS})

add_executable(TangoAccessControl ${SOURCES} ${ADDITIONAL_SOURCES})
target_link_libraries(TangoAccessControl ${TANGO_PKG_LIBRARIES} ${MYSQL_LIBRARIES} -Wl,-z,now -pie)
target_compile_options(TangoAccessControl PUBLIC ${TANGO_PKG_CFLAGS_OTHER} -Wall -Wextra -D_FORTIFY_SOURCE=2 -O1 -fpie)

install(TARGETS TangoAccessControl
        RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
        CONFIGURATIONS ${CMAKE_BUILD_TYPE})
